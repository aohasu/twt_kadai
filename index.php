<?php
  session_start();
  if($_SESSION['login'] === true){
    $login = true;
  } else {
    $login = false;
  }

?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>aohasu.net</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./style/style.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="index.php">aohasu.net</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">TimeLine</a></li>
              <li><a href="#">mention</a></li>
              <li><a href="#">talk</a></li>
            </ul>
          <form class="navbar-form pull-left" action="">
            <input type="text" class="search-query span4" placeholder="Search">
          </form>
          <ul class="nav pull-right">
          <?php
            if($login){
              echo '<li><a href="#">＠'.$_SESSION['screen_name'].'</a></li>';
              echo '<li><a href="./logout.php">ログアウト</a></li>';
            } else {
              echo '<li><a href="./login.php">Twitterアカウントでログイン</a></li>';
            }
          ?>
          </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <?php
      if($login){
        require('./mypage.php');
      } else {
        require('./guest.php');
      }
    ?>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--
    <script src="http://cdn.socket.io/stable/socket.io.js"></script>
    -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

  </body>
</html>

