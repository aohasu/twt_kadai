<?php
  /*
    jsonの読み込み・書き込み

    $jsonData = new json('filename'); // json読み込み
    echo $jsonData->data['test']; // データ参照
    $jsonData->save(); //データ書き込み
  */

  class json{
    public $data;
    private $filename;

    public function __construct($name){
      $this->filename = $name;
      if(file_exists($this->filename)){
        $this->data = json_decode(file_get_contents($this->filename), true);
      } else {
        $this->data = array();
      }
    }

    public function save(){
      file_put_contents($this->filename, json_encode($this->data));
    }
  }
?>
