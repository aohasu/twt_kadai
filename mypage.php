<?php
  require_once('./twitter.php');

  $twObj = getTwitterObj();
?>

<div class="container">
  <div class="tweet_form">
    <h1>Twitter</h1>
    <form class="well" action="./post.php" method="POST">
      <textarea name="tweet" class="span10" rows="3"></textarea>
      <button type="submit" class="btn" rows="3">Post</button>
    </form>
  </div>

  <?php
    $getLimit = 50;
    $req = $twObj->OAuthRequest(
      "https://api.twitter.com/1.1/statuses/home_timeline.json",
      "GET",
      array("count"=>$getLimit)
    );

    $result = json_decode($req);

    if (isset($result)) {
      foreach ($result as $status) {
        $status_id = $status->id_str;
        $text = $status->text;
        $user_id = $status->user->screen_name;
        $user_name = $status->user->name;
        $user_icon = $status->user->profile_image_url;
        $time = date('m-d  H:i:s', strtotime($status->created_at));

        $retweeted = $status->retweeted;

        if($user_id === $_SESSION['screen_name']){ //自分のツイート
          echo '<div class="mypost status">'."\n";
        } else if($retweeted) { // リツイート
          echo '<div class="status retweet">'."\n";
        } else { // 他人のツイート
          echo '<div class="status">'."\n";
        }

        echo '  <div class="icon">'."\n";
        echo '    <a herf="http://twitter.com/'.$user_id.'"><img src="'.$user_icon.'"></a>'."\n";
        echo '  </div>'."\n";
        echo '  <div class="tweet">'."\n";
        echo '    <div class="user">'."\n";
        echo '      ＠<a herf="http://twitter.com/'.$user_id.'">'.$user_id.'</a> - '.$user_name."\n";
        echo '    </div>'."\n";
        echo '    <div class="text">'."\n";
        echo '      '.$text;
        echo '    </div>'."\n";
        echo '    <div class="time">'."\n";
        echo '      '.$time;
        echo '    </div>'."\n";
        echo '  </div>'."\n";
        echo '</div>'."\n";
      }
    }
  ?>
</div>
