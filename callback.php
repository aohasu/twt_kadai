<?php
  session_start();
  $request_token = $_SESSION["request_token"];

  require_once("./twitteroauth/twitteroauth.php");
  require_once('./twitter.php');

  $CONSUMER = getConsumer();

  $twitter = new TwitterOAuth(
    $CONSUMER['key'],
    $CONSUMER['secret'],
    $request_token["oauth_token"],
    $request_token["oauth_token_secret"]
  );

  $access_token = $twitter->getAccessToken();

  $_SESSION['access_token'] = $access_token;
  $_SESSION['login'] = true;

  $_SESSION['screen_name'] = $access_token['screen_name'];

  header('Location: /school/kadai/twt_kadai/index.php');
?>
