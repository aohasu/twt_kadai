<?php
  session_start();
  require_once('./twitteroauth/twitteroauth.php');
  require_once('./twitter.php');

  $CONSUMER = getConsumer();

  $twitter = new TwitterOauth($CONSUMER['key'], $CONSUMER['secret']);
  $req_token = $twitter->getRequestToken();

  $_SESSION['request_token'] = $req_token;

  $authUrl = $twitter->getAuthorizeURL($req_token['oauth_token'], true);

  header("Location: " . $authUrl);
?>
