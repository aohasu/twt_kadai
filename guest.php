    <div class="container">
      <div class="hero-unit">
        <h1>
          ブラウザで使えるTwitterクライアント
        </h1>
        <p>
          シンプルで簡単に使えて、でもちょっと便利、そんなクライアントを目指します。
        </p>
      </div>
      <div class="row">
        <div class="span6">
          <h2>カラフルで見やすく</h2>
          <p>
            自分の発言、mention、会話を関連付けて色分けして表示します。<br>
            <br>
            実装度25%
          </p>
        </div>
        <div class="span6">
          <h2>この発言、気になる</h2>
          <p>
            気になる発言をタイムラインの片隅にメモしておくことができます。<br>
            <br>
            未実装
          </p>
        </div>
      </div>
    </div>
