<?php
  require_once('./twitteroauth/twitteroauth.php');

  // コンシューマーキーを返す、本来はjson読み込み
  function getConsumer(){
    // $CONSUMER = json_decode(file_get_contents('./configure.json', true));
    $CONSUMER = array(
      "key" => "ovBDoWgfRXHxJAaFc3Atg",
      "secret" => "PclSN2z0yDRF1nsNUkrOteJGcUSx28qDgfHiz7ouU"
    );
    return $CONSUMER;
  }

  // 認証済みTwitterインスタンスを返す
  function getTwitterObj(){
    $CONSUMER = getConsumer();

    if(!isset($_SESSION['access_token'])) return null;

    $token = $_SESSION['access_token'];
    $ACCESS_TOKEN = $token['oauth_token'];
    $ACCESS_TOKEN_SECRET = $token['oauth_token_secret'];

    $twObj = new TwitterOAuth(
      $CONSUMER['key'],
      $CONSUMER['secret'],
      $ACCESS_TOKEN,
      $ACCESS_TOKEN_SECRET
    );

    return $twObj;
  }
?>
